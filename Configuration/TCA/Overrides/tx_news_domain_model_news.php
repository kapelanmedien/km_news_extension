<?php
defined('TYPO3_MODE') or defined('TYPO3') or die();

$tempColumns = [
    'tx_kmnewsextension_contacts' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:km_news_extension/Resources/Private/Language/locallang_db.xlf:tx_news_domain_model_news.tx_kmnewsextension_contacts',
        'config' => [
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => 'tt_address',
            'foreign_table' => 'tt_address',
            'MM' => 'tx_news_domain_model_news_ttaddress_mm',
            'filter' => [
                [
                    'userFunc' => \KapelanMedien\KmNewsExtension\Utility\AddressFilter::class . '->doFilter',
                    'parameters' => [],
                ],
            ],
        ]
    ],
    'tx_kmnewsextension_footer' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:km_news_extension/Resources/Private/Language/locallang_db.xlf:tx_news_domain_model_news.tx_kmnewsextension_footer',
        'config' => [
            'type' => 'text',
            'cols' => '40',
            'rows' => '15',
            'eval' => 'trim',
            'enableRichtext' => true,
        ]
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $tempColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'tx_kmnewsextension_contacts, tx_kmnewsextension_footer');

// Set renderType to be able to set values automatically
$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['author']['config']['renderType'] = 'inputAuthor';
$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['author_email']['config']['renderType'] = 'inputAuthorMail';
