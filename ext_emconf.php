<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "km_news_extension".
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Extension of EXT:news',
    'description' => 'Extends EXT:news records by e.g. adding EXT:tt_address contacts.',
    'category' => 'misc',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearcacheonload' => 1,
    'author' => 'Uwe Wiebach',
    'author_email' => 'wiebach@kapelan.com',
    'author_company' => 'Kapelan Medien GmbH',
    'version' => '2.1.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.13-11.5.99',
            'news' => '9.0.0-',
            'tt_address' => '6.0.0-',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'KapelanMedien\\KmNewsExtension\\' => 'Classes'
        ]
    ]
];
