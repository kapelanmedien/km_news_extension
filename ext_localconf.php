<?php
defined('TYPO3_MODE') or defined('TYPO3') or die();

(function () {
    /***************
     * Extend EXT:news
     */
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/News'][] = 'km_news_extension';
    
    
    /***************
     * Register renderTypes
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1708005093] = [
        'nodeName' => 'inputAuthor',
        'priority' => '70',
        'class' => \KapelanMedien\KmNewsExtension\Form\Element\InputTextElement::class,
    ];
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1708005094] = [
        'nodeName' => 'inputAuthorMail',
        'priority' => '70',
        'class' => \KapelanMedien\KmNewsExtension\Form\Element\InputTextElement::class,
    ];
    
    
    /***************
     * Localization
     */
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:news/Resources/Private/Language/locallang.xlf'][1708005093] = 'EXT:km_news_extension/Resources/Private/Language/locallang.xlf';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:news/Resources/Private/Language/locallang.xlf'][1708005093] = 'EXT:km_news_extension/Resources/Private/Language/de.locallang.xlf';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:news/Resources/Private/Language/locallang.xlf'][1708005094] = 'EXT:tt_address/Resources/Private/Language/locallang.xlf';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:news/Resources/Private/Language/locallang.xlf'][1708005094] = \TYPO3\CMS\Core\Core\Environment::getLabelsPath() . '/de/tt_address/Resources/Private/Language/de.locallang.xlf';
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('km_ttaddress_extension')) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:news/Resources/Private/Language/locallang.xlf'][1708005095] = 'EXT:km_ttaddress_extension/Resources/Private/Language/locallang.xlf';
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:news/Resources/Private/Language/locallang.xlf'][1708005095] = 'EXT:km_ttaddress_extension/Resources/Private/Language/de.locallang.xlf';
    }
})();
