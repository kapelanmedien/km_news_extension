<?php

namespace KapelanMedien\KmNewsExtension\Form\Element;

use TYPO3\CMS\Backend\Form\Element\InputTextElement as StandardInputTextElement;

/*
 * This file is part of the "km_news_extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Extend general type=input element.
 */
class InputTextElement extends StandardInputTextElement
{
    /**
     * This will render a single-line input form field and set default values from current user
     *
     * @return array As defined in initializeResultArray() of AbstractNode
     */
    public function render(): array
    {
        $resultArray = parent::render();
        $parameterArray = $this->data['parameterArray'];
        if (strpos($parameterArray['itemFormElID'], 'NEW') !== false) {
            $config = $parameterArray['fieldConf']['config'];
            if ($config['renderType'] === 'inputAuthor') {
                $resultArray['html'] = str_replace('value=""', 'value="' . $GLOBALS['BE_USER']->user['realName'] . '"', $resultArray['html']);
            } else if ($config['renderType'] === 'inputAuthorMail') {
                $resultArray['html'] = str_replace('value=""', 'value="' . $GLOBALS['BE_USER']->user['email'] . '"', $resultArray['html']);
            }
        }
        return $resultArray;
    }
}
