<?php

namespace KapelanMedien\KmNewsExtension\Domain\Model;

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/*
 * This file is part of the "km_news_extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Class News
 *
 * @package KapelanMedien\KmNewsExtension\Domain\Model
 */
class News extends \GeorgRinger\News\Domain\Model\News {

    /**
     * contact
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\FriendsOfTYPO3\TtAddress\Domain\Model\Address>
     *
     */
    protected $txKmnewsextensionContacts;

    /**
     * footer
     *
     * @var string
     */
    protected $txKmnewsextensionFooter;

    /**
     * Returns the contacts
     * 
     * @return ObjectStorage
     */
    public function getTxKmnewsextensionContacts(): ObjectStorage
    {
        return $this->txKmnewsextensionContacts;
    }

    /**
     * Sets the contacts
     * 
     * @param ObjectStorage $txKmnewsextensionContacts
     * @return void
     */
    public function setTxKmnewsextensionContacts($txKmnewsextensionContacts): void
    {
        $this->txKmnewsextensionContacts = $txKmnewsextensionContacts;
    }

    /**
     * Returns the footer
     *
     * @return string $txKmnewsextensionFooter
     */
    public function getTxKmnewsextensionFooter(): string
    {
        return $this->txKmnewsextensionFooter;
    }

    /**
     * Sets the footer
     *
     * @param string $txKmnewsextensionFooter
     * @return void
     */
    public function setTxKmnewsextensionFooter($txKmnewsextensionFooter): void
    {
        $this->txKmnewsextensionFooter = $txKmnewsextensionFooter;
    }

}
