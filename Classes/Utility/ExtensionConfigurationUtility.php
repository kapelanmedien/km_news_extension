<?php
namespace KapelanMedien\KmNewsExtension\Utility;

/*
 * This file is part of the "km_news_extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class ExtensionConfigurationUtility
{

    /**
     * Gets the configuration of the extension
     * @return array
     */
    public static function getCurrentConfiguration(): array
    {
        $extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get('km_news_extension');
        return $extensionConfiguration;
    }

    /**
     * Return whether to show hidden addresses or not
     * @return bool
     */
    public static function showHiddenAddresses(): bool
    {
        $configuration = self::getCurrentConfiguration();
        return $configuration['showHiddenAddresses'] == 1;
    }

}