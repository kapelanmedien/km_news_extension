<?php
namespace KapelanMedien\KmNewsExtension\Utility;

/*
 * This file is part of the "km_news_extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

class AddressFilter
{
    
    /**
     * Filter addresses
     * @param array $parameters
     * @param object $parentObject
     * @return array the filtered addresses
     */
    public function doFilter(array $parameters, $parentObject): array
    {
        $values = $parameters['values'];
        $cleanValues = [];
        $showHidden = \KapelanMedien\KmNewsExtension\Utility\ExtensionConfigurationUtility::showHiddenAddresses();

        if (is_array($values)) {
            foreach ($values as $value) {
                if (empty($value)) {
                    continue;
                }
                $parts = \TYPO3\CMS\Core\Utility\GeneralUtility::revExplode('_', $value, 2);
                $addressUid = $parts[count($parts) - 1];
                $address = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('tt_address', $addressUid);
                if ($showHidden || $address['hidden'] == '0') {
                    $cleanValues[] = $value;
                }
            }
        }

        return $cleanValues;
    }
    
}
