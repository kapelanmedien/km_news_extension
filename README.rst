TYPO3 Extension "km_news_extension"
===================================

This extension adds relations to the table 'tt_address' to a news record of EXT:news. And it also adds a new text field ('footer').

**Required**

- TYPO3 CMS 10.4 LTS
- EXT:news 9.0.0
- EXT:tt_address 6.0.0

**License**

GPLv2

Installation and usage
----------------------

- Install the extension from TER or via composer by running :bash:`composer require kapelanmedien/km-news-extension`
- Create a address
- Create/Open a news record and select the tt_address and/or add a footer text in the :guilabel:`Extended` tab

Templates
---------

You can output the contacts by using ``{newsItem.txKmnewsextensionContacts}`` and the footer text by ``{newsItem.txKmnewsextensionFooter}``. As an example
(can be also found at :file:`EXT:km_news_extension/Resources/Private/Frontend/Partials/`):

..  code-block:: html

    <f:if condition="{newsItem.txKmnewsextensionContacts}">
        <!-- Contacts -->
        <div class="news-contacts">
            <h4><f:translate key="contact" /></h4>
            <f:for each="{newsItem.txKmnewsextensionContacts}" as="contact">
                <f:comment>tt_address partial</f:comment>
                <f:render partial="Full" arguments="{address: contact}"/>
            </f:for>
        </div>
    </f:if>

    <f:if condition="{newsItem.txKmnewsextensionFooter}">
        <f:format.html>{newsItem.txKmnewsextensionFooter}</f:format.html>
    </f:if>

This will output the the contacts using the full partial of tt_address and the formatted footer text. In order to use the tt_address partial its path has
to be added to news settings:

..  code-block:: typoscript

    plugin.tx_news.view.partialRootPaths.2 = EXT:tt_address/Resources/Private/Partials/


